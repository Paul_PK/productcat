'use strict';

/* this file binds all majors controllers to the Main Module*/

var mainCtrl = angular.module('productcatControllers',['ngAnimate']);

	
		/*******HOME CONTROLLER*******/
	mainCtrl.controller('homeController', ['$scope', 'homeData',function($scope, homeData){
		/*Images data return from the service slider*/
		$scope.homeImages = homeData.query();
		$scope.currentIndex = 0;
		$scope.setCurrentSlideIndex = function (index) {
				$scope.currentIndex = index;
			};
		$scope.isCurrentSlideIndex = function (index) {
				return $scope.currentIndex === index;
			};
		$scope.prevSlide = function () {
				$scope.currentIndex = ($scope.currentIndex < $scope.homeImages.length - 1) ? ++$scope.currentIndex : 0;
			};

		$scope.nextSlide = function () {
				$scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.homeImages.length - 1;
			};
			
		$scope.name ="Diamond Hotel";
		$scope.locations = [{'city':'Lahti', 'address':'niemikatu 16'},	
						{'city':'Helsinki','address':'niinitie 6'},
						{'city':'Tampere','address':'kauppantie 7'},
						{'city':'Kuopio','address':'alexanterintie 5'},
						{'city':'Lapland','address':'vesijarvenkatu 1'},
		];
		$scope.message = 'Welcome.... This page only demonstrates an image slider built with AngularJS';
	}]);
	
	/********Slider animation*********/
	mainCtrl.animation('.slide-animation', function () {
        return {
            addClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    TweenMax.to(element, 0.5, {left: -element.parent().width(), onComplete: done });
                }
                else {
                    //done();
                }
            },
            removeClass: function (element, className, done) {
                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    TweenMax.set(element, { left: element.parent().width() });
                    TweenMax.to(element, 0.5, {left: 0, onComplete: done });
                }
                else {
                    //done();
                }
            }
        };
    });


	/*ROOMS CONTROLLER*/
	mainCtrl.controller('roomsListCtrl', ['$scope','roomsService','$location', function($scope, roomsService,$location){
		$scope.rooms = roomsService.query();
		$scope.setRoute = function(route){
			$location.path(route);
		};
	}]);
	
	/*BOOKING CONTROLLER*/
	mainCtrl.controller('bookingController', ['$scope', '$window','roomsService',function($scope, $window,roomsService){
		$scope.message = "Booking form coming soon!";
		$scope.rooms = roomsService.query();
		
		$scope.amounts = [{'val':0},{'val':1},{'val':2},{'val':3},{'val':4},{'val':5},{'val':6}];	
		$scope.defaultRoomSelected = $scope.rooms[1];
		$scope.defaultAmountSelected1 = $scope.amounts[0];
		$scope.defaultAmountSelected2 = $scope.amounts[0];
		
		$scope.submitForm = function(){
			if($scope.userForm.$valid)
			{	
				alert('Great Job!!!!!.Valid Inputs '); 
			}
			else{
				$scope.userForm.submitted = true;
				alert('Invalid Inputs...please check empty fields');
			}
		};

	}]);

	/*SERVICES PAGE CONTROLLER*/
	mainCtrl.controller('servicesController', function($scope){
		$scope.message = "services info coming soon!";
	});

	/*DISHES PAGE CONTROLLER*/
	mainCtrl.controller('restaurantController', function($scope){
		$scope.message = "Our dishes selection coming soon!";
	});

	/*CONTACT PAGE CONTROLLER*/
	mainCtrl.controller('contactController', function($scope){
		$scope.message = "Contact form coming soon!";
	});


  
  
