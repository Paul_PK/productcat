'use strict';

/* Controllers */

var productcatServices = angular.module('productcatApp.services',['ngResource'])
	.factory('homeData',['$resource',function($resource){
		return $resource('hotel/home.json', {},{
			query: { method:'GET', isArray:true}
		});
	}])
	.factory('roomsService',['$resource',function($resource){
		return $resource('hotel/rooms.json', {},{
			query: { method:'GET', isArray:true}
		});
	}]);
	



  
