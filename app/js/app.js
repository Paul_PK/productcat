'use strict';

/* Controllers */

var productcatApp = angular.module('productcatApp',[
	'ngRoute',
	'productcatControllers',
	'productcatAnimations',
	'productcatApp.services',
	'ValidationsApp'
	
]);

productcatApp.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
	$locationProvider.hashPrefix('!');
    $routeProvider
	  .when('/', {
        templateUrl: 'partials/home.html',
        controller: 'homeController'
      }).
	  when('/rooms', {
        templateUrl: 'partials/rooms.html',
        controller: 'roomsListCtrl'
      }).
	  when('/booking', {
        templateUrl: 'partials/booking.html',
        controller: 'bookingController'
      }).
	  when('/services', {
        templateUrl: 'partials/services.html',
        controller: 'servicesController'
      }).
	  when('/resto', {
        templateUrl: 'partials/resto.html',
        controller: 'restaurantController'
      }).
	  when('/contact', {
        templateUrl: 'partials/contact.html',
        controller: 'contactController'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);



  
